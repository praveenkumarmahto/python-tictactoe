'''
    author = Neha Shewale
    date = 2020-03-26 09:41
'''
from flask import Flask
from flask import request
from flask_restful import Resource, Api

from tictactoe.service_apis.ping import Ping

app = Flask(__name__)
api = Api(app)

app.logger.info("Setting up Resources")
api.add_resource(Ping, '/tictactoe/ping/')
app.logger.info("Resource Setup Done")

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=3010)